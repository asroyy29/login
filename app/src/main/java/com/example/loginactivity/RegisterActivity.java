package com.example.loginactivity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editUsernameReg, editPasswordReg, editEmail, editPhone;
    Button btnReg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //getActionBar().setDisplayHomeAsUpEnabled(true);
        //getActionBar().setTitle("Register Menu");

        editUsernameReg = findViewById(R.id.editUsername);
        editPasswordReg = findViewById(R.id.editPassword);
        editEmail = findViewById(R.id.editEmail);
        editPhone = findViewById(R.id.editPhone);
        btnReg = findViewById(R.id.btnReg);

        btnReg.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnReg:
                Intent intent = new Intent();
                intent.putExtra("Message", editUsernameReg.getText().toString());
                setResult(99, intent);
                break;
        }
    }
}
