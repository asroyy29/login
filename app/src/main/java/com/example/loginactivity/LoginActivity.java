package com.example.loginactivity;


import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editUsername, editPassword;
    Button btnLogin;
    TextView txtReg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //getActionBar().setTitle("Login Menu");

        editUsername = findViewById(R.id.editUsername);
        editPassword = findViewById(R.id.editPassword);
        btnLogin = findViewById(R.id.btnLogin);
        txtReg = findViewById(R.id.txtReg);

        btnLogin.setOnClickListener(this);
        txtReg.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin:
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivityForResult(intent, 98);
                break;
            case R.id.txtReg:
                Intent intent1 = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivityForResult(intent1, 99);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 99){
            String message = data.getStringExtra("MESSAGE");
            editUsername.setText(message);
        }
    }
}
